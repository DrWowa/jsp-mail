# -*- coding: utf-8 -*-
#
# этот файл содержит только синтаксический сахар для использования системы
#
from jsp.mail.models import Mailing


class BaseUser(AbstractBaseUser):
    #
    # class code
    #

    def send_password_recovery(self, recovery_string):
        mailing = Mailing.objects.get(id=Mailing.ID.usr_password)
        self.save()
        context = {'private_key': recovery_string}
        return mailing.send(user=self, context=context)


class Employer(BaseUser):
    #
    # class code
    #

    def send_registration_from_admin(self, raw_password):
        mailing = Mailing.objects.get(id=Mailing.ID.emp_reg_admin)
        context = {'password': raw_password}
        return mailing.send(user=self, is_now=False, context=context, from_admin=True)

    def send_registration(self, password, manual_manager_confirm=False):
        mailing = Mailing.objects.get(id=Mailing.ID.emp_reg)
        context = {'password': password, 'manual_manager_confirm': manual_manager_confirm}
        if not manual_manager_confirm:
            context['private_key'] = self.activate_sting
        return mailing.send(user=self, is_now=False, context=context)

    def send_registration_moderator(self, new_manager=False):
        mailing = Mailing.objects.get(id=Mailing.ID.mod_company_reg)
        context = {'employer': self, 'new_manager':new_manager}
        headers = {'Reply-To': self.login}
        return mailing.send(is_now=False, context=context, headers=headers)

    def send_registration_success(self):
        mailing = Mailing.objects.get(id=Mailing.ID.emp_reg_success)
        return mailing.send(user=self)

    def send_registration_manager(self):
        mailing = Mailing.objects.get(id=Mailing.ID.manager_registration)
        context = {'company': self.company, 'private_key': self.recovery_string}
        return mailing.send(user=self, is_now=False, context=context)

    def send_unconfirmed_manager(self):
        mailing = Mailing.objects.get(id=Mailing.ID.unconfirmed_manager)
        return mailing.send(users=self.company.employer_set.exclude(id=self.id, is_active=False), is_now=False)

    def send_manager_confirmed(self, manager):
        mailing = Mailing.objects.get(id=Mailing.ID.manager_confirmed)
        address = self.company.employer_set.exclude(id=self.id, is_active=False).values_list('login', flat=True)
        context = {'user': self, 'manager': manager}
        return mailing.send(address=','.join(address), context=context)

    def send_manager_success(self):
        mailing = Mailing.objects.get(id=Mailing.ID.manager_success)
        return mailing.send(user=self)

    def send_manager_declined(self):
        mailing = Mailing.objects.get(id=Mailing.ID.manager_declined)
        return mailing.send(user=self)

    def send_vacancy_published(self):
        mailing = Mailing.objects.get(id=Mailing.ID.vacancy_published)
        return mailing.send(user=self)

    def send_vacancy_declined(self, vacancy, text):
        mailing = Mailing.objects.get(id=Mailing.ID.vacancy_declined)
        context = {'vacancy': vacancy, 'text': text}
        return mailing.send(user=self, context=context)

    def send_response(self, response):
        mailing = Mailing.objects.get(id=Mailing.ID.resp_to_vacancy)
        context = {'response': response}
        return mailing.send(user=self, context=context)

    def send_response_from_admin(self, response):
        mailing = Mailing.objects.get(id=Mailing.ID.resp_to_vacancy)
        context = {'response': response}
        return mailing.send(user=self, context=context, is_now=False, from_admin=True)

    def send_response_anonym(self, context, files=[]):
        mailing = Mailing.objects.get(id=Mailing.ID.resp_to_vacancy_anonym)
        return mailing.send(user=self, context=context, files=files)

    def send_response_anonym_moderator(self, context):
        mailing = Mailing.objects.get(id=Mailing.ID.mod_resp_to_vacancy_anonym)
        return mailing.send(context=context)

    def send_response_answer(self, response):
        mailing = Mailing.objects.get(id=Mailing.ID.emp_response_answer)
        address = self.company.employer_set.values_list('login', flat=True)
        context = {'response': response}
        return mailing.send(address=','.join(address), context=context)

    def send_response_deleted(self, response):
        mailing = Mailing.objects.get(id=Mailing.ID.emp_response_deleted)
        address = self.company.employer_set.values_list('login', flat=True)
        context = {'response': response}
        return mailing.send(address=','.join(address), context=context)


class Applicant(BaseUser):
    #
    # class code
    #

    def send_regisration(self, password):
        mailing = Mailing.objects.get(id=Mailing.ID.app_reg)
        context = {'password': password, 'private_key': self.activate_sting}
        return mailing.send(user=self, is_now=False, context=context)

    def send_registration_from_admin(self):
        mailing = Mailing.objects.get(id=Mailing.ID.app_reg_admin)
        return mailing.send(user=self, is_now=False, from_admin=True)

    def send_response(self, response):
        mailing = Mailing.objects.get(id=Mailing.ID.resp_to_resume)
        context = {'response': response}
        return mailing.send(user=self, context=context)

    def send_response_answer(self, response):
        mailing = Mailing.objects.get(id=Mailing.ID.app_response_answer)
        context = {'response': response}
        return mailing.send(user=self, context=context)

    def send_response_deleted(self, response):
        mailing = Mailing.objects.get(id=Mailing.ID.app_response_deleted)
        context = {'response': response}
        return mailing.send(user=self, context=context)

    def send_subscription(self, context):
        mailing = Mailing.objects.get(id=Mailing.ID.app_subscription)
        return mailing.send(user=self, context=context)
