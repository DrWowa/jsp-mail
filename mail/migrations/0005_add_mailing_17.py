# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.
        orm.Mailing.objects.bulk_create([
        orm.Mailing(
            id=17,
            for_user=5,
            subject='Вакансия отклонена',
            template='emp/vacancy_declined',
            is_system=False
        ),
        orm.Mailing(
            id=18,
            for_user=2,
            subject='Новая вакансия',
            template='mod/vacancy_new',
            is_system=True
        ),
        orm.Mailing(
            id=19,
            for_user=2,
            subject='Изменена вакансия',
            template='mod/vacancy_changed',
            is_system=True
        ),
        orm.Mailing(
            id=20,
            for_user=2,
            subject='Восстановлена архивированная вакансия',
            template='mod/vacancy_from_archive',
            is_system=True
        ),
        orm.Mailing(
            id=21,
            for_user=5,
            subject='Регистрация менеджера',
            template='emp/registration_manager',
            is_system=False
        ),
        orm.Mailing(
            id=22,
            for_user=5,
            subject='Подтверждён новый сотрудник',
            template='emp/confirmed_manager',
            is_system=False
        ),
        orm.Mailing(
            id=23,
            for_user=5,
            subject='Вы успешно приняты',
            template='emp/manager_success',
            is_system=False
        ),
        orm.Mailing(
            id=24,
            for_user=5,
            subject='Запрос отклонён',
            template='emp/manager_declined',
            is_system=True
        ),
        orm.Mailing(
            id=25,
            for_user=2,
            subject='Изменен статус поиска работы в резюме',
            template='mod/complaint_resume_status',
            is_system=True
        ),
        orm.Mailing(
            id=26,
            for_user=4,
            subject='Статус вашего резюме принудительно изменен',
            template='app/complaint_resume_status',
            is_system=False
        ),
        orm.Mailing(
            id=27,
            for_user=2,
            subject='Подана жалоба на резюме',
            template='mod/complaint_resume',
            is_system=True
        ),
        orm.Mailing(
            id=28,
            for_user=2,
            subject='Подана жалоба на вакансию',
            template='mod/complaint_vacancy',
            is_system=True
        ),
        orm.Mailing(
            id=29,
            for_user=2,
            subject='На вакансию пожаловались уже 5 раз',
            template='mod/complaint_vacancy_status',
            is_system=True
        ),
        orm.Mailing(
            id=30,
            for_user=4,
            subject='Автоподбор вакансий',
            template='app/autofit',
            is_system=False
        ),
        orm.Mailing(
            id=31,
            for_user=4,
            subject='Обновите своё резюме',
            template='app/update_resume',
            is_system=False
        ),
        orm.Mailing(
            id=32,
            for_user=4,
            subject='Узнайте, как улучшить свое резюме',
            template='app/improve_resume',
            is_system=False
        ),
        orm.Mailing(
            id=33,
            for_user=3,
            subject='Вакансия на Foundjob.ru',
            template='usr/shared_vacancy',
            is_system=True
        ),
        orm.Mailing(
            id=34,
            for_user=2,
            subject='Новое пожелание или предложение',
            template='mod/support',
            is_system=True
        ),
        orm.Mailing(
            id=35,
            for_user=5,
            subject='Начните поиск сотрудников уже сегодня!',
            template='emp/search_persons_today',
            is_system=False
        ),
        orm.Mailing(
            id=36,
            for_user=5,
            subject='Новые резюме на сайте Foundjob.ru',
            template='emp/autofit',
            is_system=False
        ),
        orm.Mailing(
            id=37,
            for_user=4,
            subject='Новые вакансии!',
            template='app/subscription',
            is_system=False
        ),
        orm.Mailing(
            id=38,
            for_user=6,
            subject='Новая заявка на услугу',
            template='mod/request_to_service',
            is_system=True
        ),
        orm.Mailing(
            id=39,
            for_user=4,
            subject='Добро пожаловать!',
            template='app/registration_from_admin',
            is_system=True
        ),
        ])

    def backwards(self, orm):
        "Write your backwards methods here."
        orm.Mailing.objects.filter(id__in=range(17,40)).delete()

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mail.brokenaddress': {
            'Meta': {'object_name': 'BrokenAddress'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mail.email': {
            'Meta': {'object_name': 'Email'},
            'body_email': ('django.db.models.fields.TextField', [], {}),
            'context': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'date_sent': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 11, 0, 0)'}),
            'from_email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'headers': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_sent': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'mailing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mail.Mailing']"}),
            'reply_code': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '250'}),
            'reply_text': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'}),
            'to_email': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.BaseUser']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'})
        },
        u'mail.mailing': {
            'Meta': {'object_name': 'Mailing'},
            'for_user': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mail.unsubscriber': {
            'Meta': {'unique_together': "((u'user', u'mailing'),)", 'object_name': 'Unsubscriber'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mailing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mail.Mailing']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.BaseUser']"})
        },
        u'users.baseuser': {
            'Meta': {'object_name': 'BaseUser'},
            'activate_sting': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'added_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.BaseUser']", 'null': 'True', 'blank': 'True'}),
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'enable_chat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'enable_comments': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'from_source': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'from_source_other': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'last_seen_online': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'login': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'patronymic': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'receive_sms': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'recovery_string': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        }
    }

    complete_apps = ['mail']
    symmetrical = True
