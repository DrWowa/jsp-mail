# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Mailing'
        db.create_table(u'mail_mailing', (
            ('id', self.gf('django.db.models.fields.PositiveSmallIntegerField')(primary_key=True)),
            ('for_user', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('template', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('is_system', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'mail', ['Mailing'])

        # Adding model 'BrokenAddress'
        db.create_table(u'mail_brokenaddress', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75)),
        ))
        db.send_create_signal(u'mail', ['BrokenAddress'])

        # Adding model 'Email'
        db.create_table(u'mail_email', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mailing', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mail.Mailing'])),
            ('date_sent', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2015, 2, 9, 0, 0))),
            ('from_email', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('to_email', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('body_email', self.gf('django.db.models.fields.TextField')()),
            ('reply_code', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=250)),
            ('reply_text', self.gf('django.db.models.fields.CharField')(max_length=80, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.BaseUser'], null=True, on_delete=models.SET_NULL, blank=True)),
            ('is_sent', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('context', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('headers', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'mail', ['Email'])

        # Adding model 'Unsubscriber'
        db.create_table(u'mail_unsubscriber', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.BaseUser'])),
            ('mailing', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mail.Mailing'])),
        ))
        db.send_create_signal(u'mail', ['Unsubscriber'])

        # Adding unique constraint on 'Unsubscriber', fields ['user', 'mailing']
        db.create_unique(u'mail_unsubscriber', ['user_id', 'mailing_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Unsubscriber', fields ['user', 'mailing']
        db.delete_unique(u'mail_unsubscriber', ['user_id', 'mailing_id'])

        # Deleting model 'Mailing'
        db.delete_table(u'mail_mailing')

        # Deleting model 'BrokenAddress'
        db.delete_table(u'mail_brokenaddress')

        # Deleting model 'Email'
        db.delete_table(u'mail_email')

        # Deleting model 'Unsubscriber'
        db.delete_table(u'mail_unsubscriber')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mail.brokenaddress': {
            'Meta': {'object_name': 'BrokenAddress'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mail.email': {
            'Meta': {'object_name': 'Email'},
            'body_email': ('django.db.models.fields.TextField', [], {}),
            'context': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'date_sent': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 9, 0, 0)'}),
            'from_email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'headers': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_sent': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'mailing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mail.Mailing']"}),
            'reply_code': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '250'}),
            'reply_text': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'}),
            'to_email': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.BaseUser']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'})
        },
        u'mail.mailing': {
            'Meta': {'object_name': 'Mailing'},
            'for_user': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'is_system': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mail.unsubscriber': {
            'Meta': {'unique_together': "((u'user', u'mailing'),)", 'object_name': 'Unsubscriber'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mailing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mail.Mailing']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.BaseUser']"})
        },
        u'users.baseuser': {
            'Meta': {'object_name': 'BaseUser'},
            'activate_sting': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'added_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.BaseUser']", 'null': 'True', 'blank': 'True'}),
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'last_seen_online': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'login': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'patronymic': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'receive_sms': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'recovery_string': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        }
    }

    complete_apps = ['mail']