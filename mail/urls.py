# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from views import UnsubscribeFromMaillistView


urlpatterns = patterns('',
    url(r'^unsubscribe/(?P<user_id>\d+)-(?P<mailing_id>\d+)-(?P<hash>\w+)/$',
        UnsubscribeFromMaillistView.as_view(), name='unsubscribe'),
    )
