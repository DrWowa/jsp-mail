# -*- coding: utf-8 -*-
import pickle
from base64 import b64decode
from smtplib import (SMTPDataError, SMTPRecipientsRefused,
                     SMTPSenderRefused, SMTPServerDisconnected)

from celery.exceptions import MaxRetriesExceededError
import celery

from django.conf import settings
from django.core.mail import get_connection
from django.utils import timezone
from jsp.mail.models import BrokenAddress, Email
from jsp.mail import FjEmail
from jsp.celery import app


CONFIG = getattr(settings, 'CELERY_EMAIL_TASK_CONFIG', {})
BACKEND = getattr(settings, 'CELERY_EMAIL_BACKEND',
                  'django.core.mail.backends.smtp.EmailBackend')
TASK_CONFIG = {
    'name': 'mail.send_emails',
    'ignore_result': True,
}
TASK_CONFIG.update(CONFIG)


@celery.task(**TASK_CONFIG)
def send_emails(messages, **kwargs):
    """Отправка почты, сохранение статистики и обработка ответов сервера"""
    def stat_logger(msg, lgr, err=None):
        if err:
            msg.reply_code = err.smtp_code
            msg.reply_text = err.smtp_error
            lgr.warning(err)
        else:
            lgr.debug("Successfully sent email message to %r.", msg.to)
        try:
            msg.is_sent = True
            msg.save_stat()
        except AttributeError:
            pass

    logger = send_emails.get_logger()
    conn = get_connection(backend=BACKEND, **kwargs)
    dt_now = timezone.now()
    num_sent = 0
    retry_msg = []
    for msg in messages:
        msg.date_sent = dt_now
        try:
            # пытаемся отправить письмо
            conn.send_messages([msg])
        except SMTPRecipientsRefused as e:
            # все адреса получателей -- фейки
            # Статистику нужно сохранить до блеклиста
            e.smtp_code = e.recipients[msg.to[0]][0]
            e.smtp_error = e.recipients[msg.to[0]][1]
            stat_logger(msg, logger, e)
            # Вносим в черный список
            for adr in e.recipients:
                if not BrokenAddress.objects.filter(email=adr).exists():
                    BrokenAddress.objects.create(email=adr)
            continue
        except SMTPDataError as e:
            # письмо битое или спам
            stat_logger(msg, logger, e)
            continue
        except SMTPSenderRefused as e:
            # нас побанили
            #TODO намылить админам
            stat_logger(msg, logger, e)
            continue
        except SMTPServerDisconnected as e:
            #обвалилось соединение, отправляем оставшееся повторно
            ind = messages.index(msg)
            retry_msg.extend(messages[ind:])
            logger.warning(e)
            continue
        except Exception as e:
            # что-то странное случилось, попробуем еще разик..
            retry_msg.append(msg)
            logger.warning(e)
            continue
        else:
            # успешно отправили
            num_sent += 1
            stat_logger(msg, logger)

    if retry_msg:
        if retry_msg == messages:
            try:
                send_emails.retry()
            except MaxRetriesExceededError as e:
                #TODO to stat
                pass
        else:
            send_emails(retry_msg)
    return num_sent


@app.task(name='mail.send_delayed_emails', ignore_result=True)
def send_delayed_emails():
    delayed_messages = Email.objects.filter(is_sent=False)
    if not delayed_messages:
        return 0

    messages = []
    for dld_msg in delayed_messages:
        msg = FjEmail(
            to=dld_msg.to_email.split(','),
            from_email=dld_msg.from_email,
            mailing=dld_msg.mailing,
            user=dld_msg.user,
            is_sent=dld_msg.is_sent,
            stat=dld_msg,
        )
        msg._fill_message()
        if dld_msg.context:
            msg.context.update(pickle.loads(b64decode(dld_msg.context)))
        if dld_msg.headers:
            msg.extra_headers.update(pickle.loads(b64decode(dld_msg.headers)))
        messages.append(msg)
    return get_connection().send_messages(messages)