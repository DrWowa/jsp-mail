# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, Client
from django.test.utils import override_settings
from django.core import mail
from jsp.users.models import BaseUser, Applicant, Employer
from jsp.employer.models import Company
from __init__ import FjEmail
from models import BrokenAddress, Mailing, Email
from backends.testing import BAD_SENDER, BAD_RECIPIENT, BAD_DATA, SUCCESS_REPLY


TO_VALID = ['to_test@fj.ru']
TO_NOT_VALID = ['', 'test', 'test@test', 'blacklist@fj.ru']


def create_workaround():
    """Создает сущности для тестов"""
    # Блокируем адрес
    BrokenAddress.objects.create(email='blacklist@fj.ru')
    # Создаем базовую категорию если её нет
    mln = Mailing.objects.create(
        id=1,
        for_user=1,
        subject='Письмо от Foudjob.ru',
        template='default',
        is_system=True
    )
    # Создаем письмо
    msg = FjEmail(
        subject='Тестовое письмо',
        body='Тестовое письмо',
        from_email='test@fj.ru',
        mailing=mln,
    )
    return msg


@override_settings(EMAIL_BACKEND='django.core.mail.backends.locmem.EmailBackend')
class FjEmailTest(TestCase):
    """Проверка отправки писем через стандартный тестовый бэк
        (locmem.EmailBackend)"""
    def setUp(self):
        self.m = create_workaround()

    def test_send_correct(self):
        """Отправка корректного письма"""
        m = self.m
        m.to = TO_VALID
        m.send()
        self.assertEqual(len(mail.outbox), 1)

    def test_no_mail(self):
        """Отправка пустого письма"""
        m = self.m
        m.mailing = None
        m.body = ''
        m.to = TO_VALID
        # Должно быть body or template or template_name
        self.assertRaises(AttributeError, m.send)

    def test_recipients(self):
        m = self.m
        # Невалидные адреса
        m.to = TO_NOT_VALID
        m.send()
        # Нет валидного получателя -- нет письма
        self.assertEqual(len(mail.outbox), 0)


@override_settings(EMAIL_BACKEND='jsp.mail.backends.celery.EmailBackend',
                   CELERY_EMAIL_BACKEND='jsp.mail.backends.testing.EmailBackend',
                   CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                   CELERY_ALWAYS_EAGER=True,
                   #BROKER_BACKEND='memory',
                   #CELERY_RESULT_BACKEND='memory',
                   )
class FjCeleryEmailTest(TestCase):
    """Тесты для celery.EmailBackend
        celery игнорит переопределение бэка, запускать с
        CELERY_EMAIL_BACKEND='jsp.mail.backends.testing.EmailBackend'
        в setting_local.py"""
    def setUp(self):
        self.m = create_workaround()

    def get_query(self, msg, rplc, rplt):
        to = msg.to
        if hasattr(to, '__iter__'):
            to = ','.join(to)
        return Email.objects.filter(
            mailing=msg.mailing,
            from_email=msg.from_email,
            to_email=to,
            reply_code=rplc,
            reply_text=rplt
        )

    def test_celery(self):
        """Повторяем тесты писем с новым бэком"""
        m = self.m

        #Невалидные адреса
        m.to = TO_NOT_VALID
        q = self.get_query(m, SUCCESS_REPLY['code'], SUCCESS_REPLY['text'])

        res = m.send(text=m.body)
        self.assertEqual(res, 0)
        self.assertFalse(q.exists())

        #Валидный адрес
        m.to = TO_VALID
        q = self.get_query(m, SUCCESS_REPLY['code'], SUCCESS_REPLY['text'])

        res = m.send(text=m.body)
        self.assertEqual(res, 1)
        self.assertTrue(q.exists())

    def test_standart_mail(self):
        """Отправка стандартного Djngo Email"""
        m = mail.EmailMessage(
            subject='Тестовое письмо',
            body='Тестовое письмо',
            from_email='test@fj.ru',
            to=TO_VALID
        )
        m.send()
        self.assertEqual(len(mail.outbox), 1)

    def test_SMTPRecipientsRefused(self):
        """Тестирует обработку SMTPRecipientsRefused
            получатель не существует"""
        m = self.m
        m.to = BAD_RECIPIENT['address']
        qbr = BrokenAddress.objects.filter(email=BAD_RECIPIENT['address'])
        q = self.get_query(m, BAD_RECIPIENT['code'], BAD_RECIPIENT['text'])

        self.assertFalse(qbr.exists())
        self.assertFalse(q.exists())

        res = m.send(text=m.body)
        self.assertEqual(res, 0)
        self.assertTrue(qbr.exists())
        self.assertTrue(q.exists())

    def test_SMTPDataError(self):
        """Тестирует обработку SMTPDataError
            письмо на принято сервером"""
        m = self.m
        m.to = BAD_DATA['address']
        q = self.get_query(m, BAD_DATA['code'], BAD_DATA['text'])

        self.assertFalse(q.exists())

        res = m.send(text=m.body)
        self.assertEqual(res, 0)
        self.assertTrue(q.exists())

    def test_SMTPSenderRefused(self):
        """Тестирует обработку SMTPSenderRefused
            сервер не принял отправителя"""
        m = self.m
        m.to = BAD_SENDER['address']
        q = self.get_query(m, BAD_SENDER['code'], BAD_SENDER['text'])

        self.assertFalse(q.exists())

        res = m.send(text=m.body)
        self.assertEqual(res, 0)
        self.assertTrue(q.exists())


@override_settings(EMAIL_BACKEND='jsp.mail.backends.celery.EmailBackend',
                   CELERY_EMAIL_BACKEND='jsp.mail.backends.testing.EmailBackend',
                   CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                   CELERY_ALWAYS_EAGER=True,
                   BROKER_BACKEND='memory')
class MailmanTest(TestCase):
    """Тесты для оберток, проверяют правильность генерации писем и контекста"""
    def setUp(self):
        self.m = create_workaround()


class UnsubscribeFromMaillistViewTest(TestCase):
    """Тесты для контроллера отписок"""
    def setUp(self):
        self.url = '/mail/unsubscribe/{}-{}-{}/'
        self.client = Client()

        self.base_user = BaseUser.objects.create(login='test_bu@fj.ru')
        self.applicant = Applicant.objects.create(login='test_app@fj.ru')
        self.company = Company.objects.create(name='Test company')
        self.employer = Employer.objects.create(login='test_emp@fj.ru', company=self.company)

        self.mln1 = Mailing.objects.create(
            id=1,
            for_user=3,  # BaseUser
            subject='Письмо от Foudjob.ru',
            template='default',
            is_system=True
        )
        self.mln2 = Mailing.objects.create(
            id=2,
            for_user=4,  # Applicant
            subject='Письмо от Foudjob.ru',
            template='default',
            is_system=False
        )
        self.mln3 = Mailing.objects.create(
            id=3,
            for_user=5,  # Employer
            subject='Письмо от Foudjob.ru',
            template='default',
            is_system=False
        )

    def test_1_invalid_hash(self):
        """base_user + mailing 1 (bu, sys)"""
        frm = (self.base_user.id, self.mln1.id, 'invalid_hash')
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 404)

    def test_2_bu_not_unsubscribed(self):
        """base_user + mailing 1 (bu, sys)"""
        chash = self.mln1.get_hash(self.base_user)
        frm = (self.base_user.id, self.mln1.id, chash)
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 404)

    def test_3_app_from_system(self):
        """applicant + mailing 1 (bu, sys)"""
        chash = self.mln1.get_hash(self.applicant)
        frm = (self.applicant.id, self.mln1.id, chash)
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['is_system'])
        self.assertFalse(resp.context['already_uns'])
        self.assertFalse(resp.context['success'])

    def test_4_app_success(self):
        """applicant + mailing 2 (app, not sys)"""
        chash = self.mln2.get_hash(self.applicant)
        frm = (self.applicant.id, self.mln2.id, chash)
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(resp.context['is_system'])
        self.assertFalse(resp.context['already_uns'])
        self.assertTrue(resp.context['success'])

    def test_5_app_already_unsubscribed(self):
        """applicant + mailing 2 (app, not sys)"""
        chash = self.mln2.get_hash(self.applicant)
        frm = (self.applicant.id, self.mln2.id, chash)
        self.client.get(self.url.format(*frm))
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(resp.context['is_system'])
        self.assertTrue(resp.context['already_uns'])
        self.assertFalse(resp.context['success'])

    def test_6_app_not_subscribed(self):
        """applicant + mailing 3 (emp, not sys)"""
        chash = self.mln3.get_hash(self.applicant)
        frm = (self.applicant.id, self.mln3.id, chash)
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 404)

    def test_7_emp_from_system(self):
        """employer + mailing 1 (bu, sys)"""
        chash = self.mln1.get_hash(self.employer)
        frm = (self.employer.id, self.mln1.id, chash)
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['is_system'])
        self.assertFalse(resp.context['already_uns'])
        self.assertFalse(resp.context['success'])

    def test_8_emp_not_subscribed(self):
        """employer + mailing 2 (app, not sys)"""
        chash = self.mln2.get_hash(self.employer)
        frm = (self.employer.id, self.mln2.id, chash)
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 404)

    def test_9_emp_success(self):
        """employer + mailing 3 (emp, not sys)"""
        chash = self.mln3.get_hash(self.employer)
        frm = (self.employer.id, self.mln3.id, chash)
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(resp.context['is_system'])
        self.assertFalse(resp.context['already_uns'])
        self.assertTrue(resp.context['success'])

    def test_10_emp_already_unsubscribed(self):
        """employer + mailing 3 (emp, not sys)"""
        chash = self.mln3.get_hash(self.employer)
        frm = (self.employer.id, self.mln3.id, chash)
        self.client.get(self.url.format(*frm))
        resp = self.client.get(self.url.format(*frm))
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(resp.context['is_system'])
        self.assertTrue(resp.context['already_uns'])
        self.assertFalse(resp.context['success'])
