# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import pickle
from base64 import b64encode

from django.core.mail import EmailMultiAlternatives
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.template import Context, Template
from django.template.loader import get_template
from django.contrib.sites.models import Site
from django.conf import settings
from django.utils import timezone
from pytils import translit

import models


class FjEmail(EmailMultiAlternatives):
    """Класс для email, с поддержкой шаблонов и
    валидацией адресов получателей"""
    def __init__(self, subject='', body=None, from_email=None,
                 to=None, bcc=None, connection=None, attachments=None,
                 headers=None, alternatives=None, cc=None,
                 context=None, template=None, template_name=None,
                 mailing=None, user=None, date_sent=None,
                 reply_code=None, reply_text=None,
                 safe=True, stat=None, is_sent=True, is_now=True,
                 from_admin=False):
        super(FjEmail, self).__init__(subject, body, from_email, to, bcc,
                                      connection, attachments, headers,
                                      alternatives, cc)
        self.context = context or {}
        self.template = template or None
        self.template_name = template_name or None
        # Для статистики
        self.mailing = mailing or models.Mailing.objects.get(id=1)
        self.user = user or None
        self.date_sent = date_sent or None
        self.reply_code = reply_code or None
        self.reply_text = reply_text or ''
        self.safe = safe
        self.stat = stat or None
        self.is_sent = is_sent
        self.is_now = is_now
        self.from_admin = from_admin

    def send(self, fail_silently=False, **kwargs):
        """Отправляет сообщение"""
        self._fill_message()
        self.context.update(kwargs)
        return super(FjEmail, self).send(fail_silently)

    def message(self):
        """Генерирует непосредственно отправляемое сообщение"""
        self.render_message()
        return super(FjEmail, self).message()

    def recipients(self):
        """Возвращает _всех_ получателей письма"""
        if self.to:
            self.to = self.validate_mails(self.to)
        if self.cc:
            self.cc = self.validate_mails(self.cc)
        if self.bcc:
            self.bcc = self.validate_mails(self.bcc)
        return super(FjEmail, self).recipients()

    def _fill_message(self):
        """Заполняет объект из категории и пользователя"""
        if self.user:
            chash = self.mailing.get_hash(self.user)
        else:
            chash = 'None'

        if self.mailing == models.Mailing.ID.default:
            self.subject = self.subject or self.mailing.subject
            self.template_name = self.template_name or self.mailing.template
        else:
            self.subject = self.mailing.subject
            self.template_name = self.mailing.template

        domain = Site.objects.get_current().domain
        self.context.update({
            'address': ','.join(self.to),
            'mailing': self.mailing,
            'user_id': 1,
            'hash': chash,
            'domain': domain,
        })
        if self.user:
            self.to = [self.user.login]
            self.context.update({
                'address': self.user.login,
                'user': self.user,
                'user_id': self.user.id,
            })

        mailing_slug = translit.slugify(self.mailing.get_id_display())
        date = str(timezone.now().date())
        list_id = '<' + mailing_slug + '.' + date + '.foundjob.ru>'
        headers = {
            'Reply-To': settings.EMAIL_REPLY_TO,
            'List-ID': list_id,
        }
        if not self.mailing.is_system:
            url_kwargs = {
                'user_id': self.context['user_id'],
                'mailing_id': self.mailing.id,
                'hash': chash,
            }
            url = domain + reverse('unsubscribe', kwargs=url_kwargs)

            headers.update({'List-Unsubscribe': url})

        self.extra_headers.update(headers)

    def render_message(self):
        """Создает тело письма из шаблона"""
        if self.body:
            return
        # Если вдруг понадобится отправка без категории
        if self.template:
            self.body = Template(self.template).render(Context(self.context))
        else:
            # Имя файла шаблона должно быть валидным
            self.body = get_template(self.template_name+'.txt').render(Context(self.context))
            html_body = get_template(self.template_name+'.html').render(Context(self.context))
            self.attach_alternative(html_body, 'text/html')

        if not self.body:
            raise AttributeError('template_name or template required')

    def _validate_mail(self, mail):
        """Проверка конкретного адреса"""
        try:
            validate_email(mail)
            if models.BrokenAddress.objects.filter(email=mail).exists():
                return False
            return True
        except ValidationError:
            return False

    def validate_mails(self, mails):
        """Проверка адресов"""
        kmails = []
        if hasattr(mails, '__iter__'):
            for mail in mails:
                if self._validate_mail(mail):
                    kmails.append(mail)
        else:
            if self._validate_mail(mails):
                kmails.append(mails)
        return kmails

    def _get_safe_context(self):
        """Заменяет ключи, содержащие защищенную информацию.
            Необходимо для сохранения статистики"""
        unsafe = ['password', 'private_key', 'hash']
        return self.context.update({}.fromkeys(unsafe, 'private'))

    def save_stat(self):
        """Сохраняет статистику по отправленным письмам"""
        to = self.recipients()
        if hasattr(to, '__iter__'):
            to = ','.join(to)

        self.body = None
        self.alternatives = []
        # Отправили отложенное письмо
        if self.is_sent:
            self.safe = True

        stat = self.stat or models.Email()

        if not self.safe:
            stat.context = b64encode(pickle.dumps(self.context))
            stat.headers = b64encode(pickle.dumps(self.extra_headers))
        else:
            stat.context = ''
            stat.headers = ''
        self._get_safe_context()

        stat.mailing = self.mailing
        stat.from_email = self.from_email
        stat.to_email = to
        stat.body_email = self.message()
        stat.is_sent = self.is_sent
        stat.from_admin = self.from_admin

        if self.date_sent:
            stat.date_sent = self.date_sent
        if self.reply_code:
            stat.reply_code = self.reply_code
        if self.reply_text:
            stat.reply_text = self.reply_text

        # Для устранения рекурсии
        from jsp.users.models import BaseUser
        if self.user and BaseUser.objects.filter(id=self.user.id).exists():
            stat.user = self.user

        return stat.save()
