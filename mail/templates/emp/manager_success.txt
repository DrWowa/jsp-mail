{% extends 'layouts/base_layout.txt' %}

{% block title-mail %}
Спешим сообщить вам, что компания {{ user.company.name }} успешно подтвердила вас в качестве своего сотрудника!
{% endblock %}

{% block content-mail %}
Напоминаем, что ваша почта {{ user.login }}. Используя её в качестве логина вы можете зайти в Ваш личный кабинет.
{% endblock %}
