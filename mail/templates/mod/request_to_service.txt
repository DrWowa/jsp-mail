{% extends 'layouts/base_layout.txt' %}
{% block title-mail %}
Новая заявка на услугу {{ service.get_id_display }}
{% endblock %}
{% block content-mail %}
Информация о работодателе:
ФИО: {{ employer.get_full_name }}
Телефон: {{ employer.phone }}
Компания: {{ employer.company.name }}
{% endblock %}
