{% extends 'layouts/moderator_layout.txt' %}
{% block title-mail %}
Добавлена новая вакансия.
{% endblock %}
{% block content-mail %}
Организация: {{ vacancy.company.name|safe }} {% if vacancy.company.is_agency %}(кадровое агентство){% endif %}

Имя {{ vacancy.employer.get_full_name }}

Ссылка на вакансию:
{{ domain }}{% url 'vacancy' vacancy.id %}
{% endblock %}
{% block admin-link %}
Перейти в административный интерфейс:
{{ domain }}{% url 'admin:employer_vacancy_change' vacancy.id %}
{% endblock %}
