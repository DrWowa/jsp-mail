# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import hashlib

from model_utils import Choices
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core.mail import get_connection

import __init__


class MailingForApplicantManager(models.Manager):
    def get_queryset(self):
        return super(MailingForApplicantManager, self).get_queryset().filter(
            models.Q(for_user=Mailing.USER_TYPE.applicant) |
            models.Q(for_user=Mailing.USER_TYPE.base_user)
        )


class MailingForEmployerManager(models.Manager):
    def get_queryset(self):
        return super(MailingForEmployerManager, self).get_queryset().filter(
            models.Q(for_user=Mailing.USER_TYPE.employer) |
            models.Q(for_user=Mailing.USER_TYPE.base_user)
        )


class Mailing(models.Model):
    """Описывает категории для всех отсылаемых email
        Данные вносятся посредством datamigration"""
    ID = Choices(
        (1, 'default', 'Стандартная категория писем'),
            # for_user=1,
            # subject='Письмо от Foudjob.ru',
            # template='default',
            # is_system=True
        (2, 'app_reg', 'Регистрация соискателя'),
            # for_user=4,
            # subject='Регистрация на сайте foundjob.ru',
            # template='app/registration',
            # is_system=True
        (3, 'emp_reg', 'Регистрация работодателя'),
            # for_user=5,
            # subject='Ваша заявка принята на рассмотрение',
            # template='emp/registration',
            # is_system=True
        (4, 'usr_password', 'Восстановление пароля'),
            # for_user=3,
            # subject='Восстановление пароля',
            # template='usr/password_recovery',
            # is_system=True
        (5, 'mod_company_reg', 'mod Регистрация новой компании'),
            # for_user=2,
            # subject='Регистрация новой компании',
            # template='mod/company_registration',
            # is_system=True
        (6, 'emp_reg_admin', 'Регистрация работодателя из админки'),
            # for_user=5,
            # subject='Регистрация на Foundjob.ru',
            # template='emp/registration_from_admin',
            # is_system=False
        (7, 'emp_reg_success', 'Компания подтверждена'),
            # for_user=5,
            # subject='Регистрация пройдена успешно',
            # template='emp/registration_success',
            # is_system=True
        (8, 'vacancy_published', 'Вакансия опубликована'),
            # for_user=5,
            # subject='Вакансия успешно опубликована на сайте',
            # template='emp/vacancy_published',
            # is_system=False
        (9, 'resp_to_resume', 'Отклик на резюме'),
            # for_user=4,
            # subject='Отклик на резюме',
            # template='app/response',
            # is_system=False
        (10, 'resp_to_vacancy', 'Отклик на вакансию'),
            # for_user=5,
            # subject='Отклик на вакансию',
            # template='emp/response',
            # is_system=False
        (11, 'resp_to_vacancy_anonym', 'Анонимный отклик на вакансию'),
            # for_user=5,
            # subject='Новый отклик от незарегистрированного пользователя на Foundjob.ru!',
            # template='emp/response_anonym',
            # is_system=False
        (12, 'mod_resp_to_vacancy_anonym', 'mod Анонимный отклик на вакансию'),
            # for_user=2,
            # subject='Новый отклик от анонимного пользователя',
            # template='mod/response_to_vacancy_anonym',
            # is_system=True
        (13, 'emp_response_answer', 'Ответ на отклик работодателю, рассмотрен'),
            # for_user=5,
            # subject='Ответ на отклик',
            # template='emp/response_answer',
            # is_system=False
        (14, 'emp_response_deleted', 'Ответ на отклик работодателю, удален'),
            # for_user=5,
            # subject='Ответ на отклик',
            # template='emp/response_deleted',
            # is_system=False
        (15, 'app_response_answer', 'Ответ на отклик соискателю, рассмотрен'),
            # for_user=4,
            # subject='Ответ на отклик',
            # template='app/response_answer',
            # is_system=False
        (16, 'app_response_deleted', 'Ответ на отклик соискателю, удален'),
            # for_user=4,
            # subject='Ответ на отклик',
            # template='app/response_deleted',
            # is_system=False
        (17, 'vacancy_declined', 'Вакансия отклонена'),
            # for_user=5,
            # subject='Вакансия отклонена',
            # template='emp/vacancy_declined',
            # is_system=False
        (18, 'mod_vacancy_new', 'mod Новая вакансия'),
            # for_user=2,
            # subject='Новая вакансия',
            # template='mod/vacancy_new',
            # is_system=True
        (19, 'mod_vacancy_changed', 'mod Изменена вакансия'),
            # for_user=2,
            # subject='Изменена вакансия',
            # template='mod/vacancy_changed',
            # is_system=True
        (20, 'mod_vacancy_from_archive', 'mod Вакансия из архива'),
            # for_user=2,
            # subject='Восстановлена архивированная вакансия',
            # template='mod/vacancy_from_archive',
            # is_system=True
        (21, 'manager_registration', 'Регистрация менеджера'),
            # for_user=3,
            # subject='Регистрация менеджера',
            # template='usr/registration_manager',
            # is_system=True
        (22, 'manager_confirmed', 'Подтверждён менеджер'),
            # for_user=5,
            # subject='Подтверждён новый сотрудник',
            # template='emp/confirmed_manager',
            # is_system=False
        (23, 'manager_success', 'Менеджер принят'),
            # for_user=5,
            # subject='Вы успешно приняты',
            # template='emp/manager_success',
            # is_system=False
        (24, 'manager_declined', 'Менеджер отклонен'),
            # for_user=5,
            # subject='Запрос отклонён',
            # template='emp/manager_declined',
            # is_system=True
        (25, 'mod_resume_status', 'mod Изменен статус резюме'),
            # for_user=2,
            # subject='Изменен статус поиска работы в резюме',
            # template='mod/complaint_resume_status',
            # is_system=True
        (26, 'app_resume_status', 'Изменен статус резюме'),
            # for_user=4,
            # subject='Статус вашего резюме принудительно изменен',
            # template='app/complaint_resume_status',
            # is_system=False
        (27, 'mod_complaint_resume', 'mod Жалоба на резюме'),
            # for_user=2,
            # subject='Подана жалоба на резюме',
            # template='mod/complaint_resume',
            # is_system=True
        (28, 'mod_complaint_vacancy', 'mod Жалоба на вакансию'),
            # for_user=2,
            # subject='Подана жалоба на вакансию',
            # template='mod/complaint_vacancy',
            # is_system=True
        (29, 'mod_vacancy_status', 'mod Изменен статус вакансии'),
            # for_user=2,
            # subject='На вакансию пожаловались уже 5 раз',
            # template='mod/complaint_vacancy_status',
            # is_system=True
        (30, 'app_vacancy_autofit', 'Автоподбор вакансий'),
            # for_user=4,
            # subject='Автоподбор вакансий',
            # template='app/autofit',
            # is_system=False
        (31, 'app_update_resume', 'Обновите своё резюме'),
            # for_user=4,
            # subject='Обновите своё резюме',
            # template='app/update_resume',
            # is_system=False
        (32, 'app_improve_resume', 'Улучшить своё резюме'),
            # for_user=4,
            # subject='Узнайте, как улучшить свое резюме',
            # template='app/improve_resume',
            # is_system=False
        (33, 'usr_share_resume', 'Поделиться вакансией'),
            # for_user=3,
            # subject='Вакансия на Foundjob.ru',
            # template='usr/shared_vacancy',
            # is_system=True
        (34, 'mod_support', 'Обратная связь'),
            # for_user=2,
            # subject='Обратная связь',
            # template='mod/support',
            # is_system=True
        (35, 'emp_search_persons', 'Начните поиск сотрудников'),
            # for_user=5,
            # subject='Начните поиск сотрудников уже сегодня!',
            # template='emp/search_persons_today',
            # is_system=False
        (36, 'emp_resume_autofit', 'Автоподбор резюме'),
            # for_user=5,
            # subject='Новые резюме на сайте Foundjob.ru',
            # template='emp/autofit',
            # is_system=False
        (37, 'app_subscription', 'Новые вакансии'),
            # for_user=4,
            # subject='Новые вакансии!',
            # template='app/subscription',
            # is_system=False
        (38, 'request_to_service', 'Заявка на услугу'),
            # for_user=6,
            # subject='Новая заявка на услугу',
            # template='mod/request_to_service',
            # is_system=True
        (39, 'app_reg_admin', 'Регистрация соискателя из админки'),
            # for_user=4,
            # subject='Добро пожаловать!',
            # template='app/registration_from_admin',
            # is_system=True
        (40, 'unconfirmed_manager', 'Неподтверждённый менеджер в компании'),
            # for_user=5,
            # subject='Подтвердите нового менеджера',
            # template='emp/new_manager',
            # is_system=True
    )

    USER_TYPE = Choices(
        (1, 'admin', 'Разработчик'),
        (2, 'moderator', 'Модератор'),
        (3, 'base_user', 'Пользователь'),
        (4, 'applicant', 'Соискатель'),
        (5, 'employer', 'Работодатель'),
        (6, 'service', 'Сервис'),
    )

    id = models.PositiveSmallIntegerField(
        verbose_name='ID', primary_key=True, choices=ID)
    for_user = models.PositiveSmallIntegerField(
        verbose_name='тип контакта', choices=USER_TYPE)
    subject = models.CharField(verbose_name='тема письма', max_length=70)
    template = models.CharField(verbose_name='шаблон письма', max_length=100)
    is_system = models.BooleanField(verbose_name='системное', default=False)

    objects = models.Manager()
    for_applicant = MailingForApplicantManager()
    for_employer = MailingForEmployerManager()

    def __unicode__(self):
        return self.get_id_display()

    def get_hash(self, user):
        strkey = settings.SECRET_KEY+str(self.id)+user.login
        return hashlib.md5(strkey).hexdigest()

    def _create_mail_user(self, user):
        msg = __init__.FjEmail(user=user, mailing=self, is_now=self.is_now)
        msg = self._update_message(msg)
        return msg

    def _create_mail_moderator(self):
        from jsp.users.models import BaseUser
        q = BaseUser.objects.filter(groups__name='new_comp_moderators')
        mod_list = q.values_list('login', flat=True)
        mod_list = mod_list if mod_list else [x[1] for x in settings.ADMINS]
        msg = __init__.FjEmail(mailing=self, to=mod_list, is_now=self.is_now)
        msg = self._update_message(msg)
        return msg

    def _create_mail_service(self):
        from jsp.users.models import BaseUser
        q = BaseUser.objects.filter(groups__name='service')
        mod_list = q.values_list('login', flat=True)
        mod_list = mod_list if mod_list else [x[1] for x in settings.ADMINS]
        msg = __init__.FjEmail(mailing=self, to=mod_list, is_now=self.is_now)
        msg = self._update_message(msg)
        return msg

    def _create_mail_address(self, address):
        msg = __init__.FjEmail(mailing=self, to=address.split(','), is_now=self.is_now)
        msg = self._update_message(msg)
        return msg

    def _update_message(self, msg):
        msg.from_admin = self.from_admin
        msg._fill_message()
        msg.context.update(self.context)
        msg.extra_headers.update(self.headers)
        if self.afiles:
            for afile in self.afiles:
                afile.seek(0)
                msg.attach(afile.name, afile.read(), afile.content_type)
        return msg

    def _get_context(self, address, context, headers):
        if address in context:
            self.context = context[address]
        if address in headers:
            self.headers = headers[address]

    def _exclude_unsubscribers(self, users):
        # У пользователя нет отписок
        q1 = models.Q(unsubscriber__isnull=True)
        # Отписки не от данной категории
        q2 = ~models.Q(unsubscriber__mailing=self)
        return users.filter(q1 | q2)

    def send(self, user=None, users=[], address='',
             fail_silently=False, is_now=True,
             context={}, headers={}, files=[],
             from_admin=False):
        """Генерирует и отправляет почту
            user == BaseUser instance
            users == BaseUser QuerySet
            address == email address"""
        messages = []
        self.is_now = is_now
        self.context = context
        self.headers = headers
        self.afiles = files
        self.from_admin = from_admin

        if self.for_user == self.USER_TYPE.moderator:
            messages.append(self._create_mail_moderator())
        elif self.for_user == self.USER_TYPE.service:
            messages.append(self._create_mail_service())
        elif user:
            q = Unsubscriber.objects.filter(user=user, mailing=self)
            if not self.is_system and q.exists():
                return 0

            self._get_context(user.login, context, headers)
            messages.append(self._create_mail_user(user))
        elif users:
            if not self.is_system:
                users = self._exclude_unsubscribers(users)

            for user in users:
                self._get_context(user.login, context, headers)
                messages.append(self._create_mail_user(user))
        elif address:
            self._get_context(address, context, headers)
            messages.append(self._create_mail_address(address))
        else:
            raise AttributeError('Не указаны получатели рассылки.')

        if not messages:
            return 0

        return get_connection(fail_silently).send_messages(messages)


class BrokenAddress(models.Model):
    """Хранит чёрный список адресов
        Сохраняются валидные адреса, не существующие на почтовом сервере
        На эти адреса _ничего_ не отправляется"""
    email = models.EmailField(verbose_name='email адрес', unique=True)

    def __unicode__(self):
        return self.email


class Email(models.Model):
    """Хранит статистику по отправленным email"""
    SMTP_REPLY_CODES = (
        (200, 'Nonstandard success response (200)'),
        (211, 'System status, or system help reply (211)'),
        (214, 'Help message (214)'),
        (220, 'Service ready (220)'),
        (221, 'Service closing transmission channel (221)'),
        (250, 'Requested mail action okay, completed (250)'),
        (251, 'User not local; will forward to <forward-path> (251)'),
        (252, 'Cannot VRFY user, but will accept message and attempt delivery (252)'),
        (354, 'Start mail input (354)'),
        (421, 'Service not available, closing transmission channel (421)'),
        (450, 'Requested mail action not taken: mailbox unavailable (450)'),
        (451, 'Requested action aborted: local error in processing (451)'),
        (452, 'Requested action not taken: insufficient system storage (452)'),
        (500, 'Syntax error, command unrecognised (500)'),
        (501, 'Syntax error in parameters or arguments (501)'),
        (502, 'Command not implemented (502)'),
        (503, 'Bad sequence of commands (503)'),
        (504, 'Command parameter not implemented (504)'),
        (521, '<domain> does not accept mail (521)'),
        (530, 'Access denied (530)'),
        (550, 'Requested action not taken: mailbox unavailable (550)'),
        (551, 'User not local; please try <forward-path> (551)'),
        (552, 'Requested mail action aborted: exceeded storage allocation (552)'),
        (553, 'Requested action not taken: mailbox name not allowed (553)'),
        (554, 'Transaction failed (554)'),
    )

    mailing = models.ForeignKey('Mailing', verbose_name='Категория')
    date_sent = models.DateTimeField(
        verbose_name='Дата отправки', default=timezone.now())
    from_email = models.CharField(verbose_name='От: адрес', max_length=50)
    to_email = models.CharField(verbose_name='Кому: адрес(а)', max_length=200)
    body_email = models.TextField(verbose_name='Содержание письма')
    reply_code = models.PositiveSmallIntegerField(
        verbose_name='Код ответа', choices=SMTP_REPLY_CODES, default=250)
    reply_text = models.CharField(
        verbose_name='Текст ответа', max_length=80, blank=True)
    user = models.ForeignKey(
        'users.BaseUser', verbose_name='Пользователь',
        blank=True, null=True, on_delete=models.SET_NULL)
    is_sent = models.BooleanField(verbose_name='Отправлялось', default=True)
    context = models.TextField(verbose_name='контекст', editable=False, blank=True)
    headers = models.TextField(verbose_name='заголовки', editable=False, blank=True)
    from_admin = models.BooleanField(verbose_name='Из админки', default=False)

    def __unicode__(self):
        return 'Письмо "{mailing}", на адрес {email}'.format(
            mailing=self.mailing.get_id_display(), email=self.to_email)


class Unsubscriber(models.Model):
    user = models.ForeignKey('users.BaseUser', verbose_name='Пользователь')
    mailing = models.ForeignKey('Mailing', verbose_name='Рассылка')

    class Meta:
        unique_together = ('user', 'mailing')

    def __unicode__(self):
        return '{user} отписался от {mailing}'.format(
            user=self.user.login, mailing=self.mailing.get_id_display())
