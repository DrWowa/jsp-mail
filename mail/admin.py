# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from daterange_filter.filter import DateRangeFilter

from jsp import datetime_to_local
from models import Email, Unsubscriber, BrokenAddress


class EmailAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'mailing',
        'to_email',
        'user',
        'wrap_date_sent',
        'reply_code',
        'reply_text',
        'from_email',
        'is_sent',
        'from_admin',
    ]
    raw_id_fields = ['user', 'mailing']
    list_filter = [
        ('date_sent', DateRangeFilter),
        'is_sent',
        'from_admin',
        'mailing',
    ]
    search_fields = [
        'to_email',
    ]

    def wrap_date_sent(self, inst):
        return datetime_to_local(inst.date_sent)
    wrap_date_sent.admin_order_field = 'date_sent'
    wrap_date_sent.short_description = 'дата отправки'


class BrokenAddressAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'email',
    ]
    search_fields = [
        'email',
    ]

class UnsubscriberAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'user',
        'mailing',
    ]
    raw_id_fields = ['user', 'mailing']
    search_fields = [
        'user__login',
    ]

admin.site.register(Email, EmailAdmin)
admin.site.register(BrokenAddress, BrokenAddressAdmin)
admin.site.register(Unsubscriber, UnsubscriberAdmin)
