# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView

from jsp.users.models import BaseUser
from models import Mailing, Unsubscriber


class UnsubscribeFromMaillistView(TemplateView):
    template_name = 'unsubscribe_from_list.html'

    def render_to_response(self, context, **kwargs):
        user = get_object_or_404(BaseUser, id=context['user_id'])
        mailing = get_object_or_404(Mailing, id=context['mailing_id'])
        if not context['hash'] == mailing.get_hash(user):
            raise Http404('Hash not valid')

        # Проверяем подписан ли пользователь на рассылку
        if not (user.is_applicant or user.is_employer):
            raise Http404('User not subscribed')
        elif user.is_applicant and not Mailing.for_applicant.filter(id=mailing.id).exists():
            raise Http404('User not subscribed')
        elif user.is_employer and not Mailing.for_employer.filter(id=mailing.id).exists():
            raise Http404('User not subscribed')

        context['is_system'] = False
        context['already_uns'] = False
        context['success'] = False
        # проверяем наличие такой отписки
        if mailing.is_system:
            context['is_system'] = True
        elif Unsubscriber.objects.filter(user=user, mailing=mailing).exists():
            context['already_uns'] = True
        else:
            Unsubscriber.objects.create(user=user, mailing=mailing)
            context['success'] = True

        context['mailing_name'] = mailing.get_id_display()
        return super(UnsubscribeFromMaillistView, self).render_to_response(context, **kwargs)
