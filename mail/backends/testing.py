# -*- coding: utf-8 -*-
"""
Backend for test environment.
"""
from smtplib import SMTPSenderRefused, SMTPRecipientsRefused, SMTPDataError

from django.core import mail
from django.core.mail.backends.base import BaseEmailBackend


BAD_SENDER = {
    'address': 'sender@fj.ru',
    'code': 554,
    'text': 'sender error',
}

BAD_RECIPIENT = {
    'address': 'recipient@fj.ru',
    'code': 554,
    'text': 'recipient error',
}

BAD_DATA = {
    'address': 'data@fj.ru',
    'code': 554,
    'text': 'data error',
}

SUCCESS_REPLY = {
    'code': 250,
    'text': '',
}


class EmailBackend(BaseEmailBackend):
    """A email backend for use during test sessions.

    The test connection stores email messages in a dummy outbox,
    rather than sending them out on the wire.

    The dummy outbox is accessible through the outbox instance attribute.
    """
    def __init__(self, *args, **kwargs):
        super(EmailBackend, self).__init__(*args, **kwargs)
        if not hasattr(mail, 'outbox'):
            mail.outbox = []

        self.raises = {
            BAD_SENDER['address']: self._raise_sender,  # In 'To' header! Not 'From'!
            BAD_RECIPIENT['address']: self._raise_recipient,
            BAD_DATA['address']: self._reise_data
        }

    def send_messages(self, messages):
        """Redirect messages to the dummy outbox"""
        sent_messages = []
        for message in messages:
            if not message.recipients():
                continue
            for to in message.to:
                try:
                    self.raises[to]()
                except KeyError:
                    message.message()  # .message() triggers header validation
                    sent_messages.append(message)
        mail.outbox.extend(sent_messages)
        return len(sent_messages)

    def _raise_sender(self):
        raise SMTPSenderRefused(
            BAD_SENDER['code'],
            BAD_SENDER['text'],
            BAD_SENDER['address']
        )

    def _raise_recipient(self):
        raise SMTPRecipientsRefused({
            BAD_RECIPIENT['address']: (BAD_RECIPIENT['code'], BAD_RECIPIENT['text'])
        })

    def _reise_data(self):
        raise SMTPDataError(BAD_DATA['code'], BAD_DATA['text'])
