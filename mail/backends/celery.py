# -*- coding: utf-8 -*-
from django.core.mail.backends.base import BaseEmailBackend

from jsp.mail.tasks import send_emails


class EmailBackend(BaseEmailBackend):
    def __init__(self, fail_silently=False, **kwargs):
        super(EmailBackend, self).__init__(fail_silently)
        self.init_kwargs = kwargs

    def send_messages(self, email_messages):
        try:
            is_now = email_messages[0].is_now
        except AttributeError:
            is_now = True

        if is_now:
            sent = send_emails.delay(email_messages, **self.init_kwargs)
            result = sent.result if sent.result else 0
        else:
            for msg in email_messages:
                msg.safe = False
                msg.is_sent = False
                msg.save_stat()
            result = 0
        return result
