# Mailing system for foundjob.ru #

This repository contains Django application for massive mailing list. It was developed for meet following requirements:

* [Yandex requirements for honest mailing lists](https://yandex.com/support/mail/spam/honest-mailers.xml)
* [ASTA recommendations](http://www.uceprotect.net/downloads/asta.pdf)
* [RFC 5322](https://tools.ietf.org/html/rfc5322)
* [RFC 2369](https://tools.ietf.org/html/rfc2369)

This application deep-integrated into [foundjob.ru](https://foundjob.ru/) project

## Uses / Requirements ##

* Python 2.7
* Django 1.6
* Celery 3

